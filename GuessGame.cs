﻿using System:
namespace GuessGame
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int min = 1;
            int max = 20;
            int number;
            int guess;
            bool playAgain = true;
            string response;

            while (playAgain)
            {
                Random random = new Random();
                number = random.Next(min, max + 1);
                int guesses = 0;
                guess = 0;

                while (guess != number)
                {
                    Console.WriteLine("Guess a number between " + min + " - "+ max + ":");
                    guess = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Guess: "+ guess);
                    if (guess > number)
                    {
                        Console.WriteLine(guess + " is to high!");
                    }
                    else if (guess < number)
                    {
                        Console.WriteLine(guess + " is to low!");
                    }

                    guesses++;
                }
                Console.WriteLine("Number is : "+ number);
                Console.WriteLine("You WIN! ");
                Console.WriteLine("Total Guesses took: " + guesses);
                Console.WriteLine("Would you like to play again (Y/N) : ");
                response = Console.ReadLine();
                response = response.ToUpper();

                if (response == "Y")
                {
                    playAgain = true;
                }
                else
                {
                    playAgain = false;
                }

            }
            Console.WriteLine("Thanks for playing !!!");
        }
    }
}