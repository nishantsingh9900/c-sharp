﻿using System;
namespace RPS_Game
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to ROCK, PAPER, SCISSORS GAME !");
            Random random = new Random();

            string player;
            string computer;
            bool playAgain = true;
            string response;

            while (playAgain)
            {
                player = "";
                computer = "";
                while (player != "ROCK" && player != "PAPER" && player != "SCISSORS")
                {
                    Console.WriteLine("\nEnter ROCK, PAPER, SCISSORS: ");
                    player = Console.ReadLine();
                    player = player.ToUpper();
                }

                switch (random.Next(0,3))
                {
                    case 0:
                        computer = "ROCK";
                        break;
                    case 1:
                        computer = "PAPER";
                        break;
                    case 2:
                        computer = "SCISSORS";
                        break;
                }

                Console.WriteLine("Player: " + player);
                Console.WriteLine("Computer: " + computer);
                switch (player)
                {
                    case "ROCK":
                        if (computer == "ROCK")
                        {
                            Console.WriteLine("It is a draw !");
                        }
                        else if (computer =="PAPER")
                        {
                            Console.WriteLine("You lose !");
                        }
                        else
                        {
                            Console.WriteLine("You win !");
                        }
                        break;

                    case "PAPER":
                        if (computer == "ROCK")
                        {
                            Console.WriteLine("You win !");
                        }
                        else if (computer == "PAPER")
                        {
                            Console.WriteLine("It is a draw !");
                        }
                        else
                        {
                            Console.WriteLine("You lose !");
                        }
                        break;

                    case "SCISSORS":
                        if (computer == "ROCK")
                        {
                            Console.WriteLine("You lose !");
                        }
                        else if (computer == "PAPER")
                        {
                            Console.WriteLine("You win !");
                        }
                        else
                        {
                            Console.WriteLine("You lose !");
                        }
                        break;
                }
                Console.WriteLine("Thanks for playing!!!\n");
                Console.WriteLine("Would you like to play again (Y/N): ");
                response = Console.ReadLine();
                response = response.ToUpper();
                if(response =="Y")
                {
                    playAgain = true;
                }
                else
                {
                    playAgain = false;
                }
                



            }





        }
    }
}